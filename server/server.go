package server

import (
	"log"
	"net"
)

const (
	FINGERPROTO = "tcp"
	FINGERPORT  = ":7779"
)

// Run starts a TCP server on port 79, listens for incoming connections,
// and passes the connection off to handleConnection
func Run() {
	server, err := net.Listen(FINGERPROTO, FINGERPORT)
	defer server.Close()

	if err != nil {
		log.Fatalf("Error listening for connections: %v\n", err)
	}

	// serve connections
	for {
		conn, err := server.Accept()
		if err != nil {
			log.Fatalf("Error accepting connections: %v\n", err)
		}

		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
}
