package server

import (
	"net"
	"testing"
	"time"
)

func TestRun(t *testing.T) {
	go Run()

	// wait for server to start - better way to do this?
	time.Sleep(100 * time.Millisecond)

	conn, err := net.Dial("tcp", ":7779")
	if err != nil {
		t.Error(err)
	}
	conn.Close()
}
